msgid ""
msgstr ""
"Project-Id-Version: gpxedit\n"
"Report-Msgid-Bugs-To: translations\\@example.com\n"
"POT-Creation-Date: 2020-02-16 13:46+0100\n"
"PO-Revision-Date: 2020-02-16 13:11\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Czech\n"
"Language: cs_CZ\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 3;\n"
"X-Crowdin-Project: gpxedit\n"
"X-Crowdin-Language: cs\n"
"X-Crowdin-File: /master/translationfiles/templates/gpxedit.pot\n"

#: /var/www/html/n18/apps/gpxedit/appinfo/app.php:43
#: /var/www/html/n18/apps/gpxedit/specialAppInfoFakeDummyForL10nScript.php:2
#: /var/www/html/n18/apps/gpxedit/templates/admin.php:7
msgid "GpxEdit"
msgstr "GpxEdit"

#: /var/www/html/n18/apps/gpxedit/js/admin.js:15
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:403
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1301
#: /var/www/html/n18/apps/gpxedit/templates/admin.php:27
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:165
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:209
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:255
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:305
msgid "Delete"
msgstr "Smazat"

#: /var/www/html/n18/apps/gpxedit/js/filetypes.js:11
msgid "Load in GpxEdit"
msgstr "Načíst v GpxEdit"

#: /var/www/html/n18/apps/gpxedit/js/filetypes.js:36
msgid "Edit with GpxEdit"
msgstr "Upravit pomocí GpxEdit"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:399
msgid "Draw a track"
msgstr "Nakreslit stopu"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:400
msgid "Add a waypoint"
msgstr "Přidat bod trasy"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:401
msgid "Edit"
msgstr "Upravit"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:402
msgid "Nothing to edit"
msgstr "Nic k úpravě"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:404
msgid "Nothing to delete"
msgstr "Nic ke smazání"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:405
msgid "Validate changes"
msgstr "Ověřit změny"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:406
msgid "Ok"
msgstr "Ok"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:407
msgid "Discard all changes"
msgstr "Zahodit všechny změny"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:408
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:416
msgid "Cancel"
msgstr "Zrušit"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:409
msgid "Drag to move elements,<br/>click to remove a point<br/>hover a middle marker and press \"Del\" to cut the line"
msgstr "Přetažením přesuňte prvky,<br/>klepnutím odstraníte bod<br/>přejdete na prostřední značku a stiskněte tlačítko \"D\" pro přerušení čáry"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:410
msgid "Click cancel to undo changes"
msgstr "Klikněte na Storno pro vrácení změn"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:411
msgid "Click on an element to delete it"
msgstr "Klikněte na prvek pro jeho smazání"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:412
msgid "Click map to add waypoint"
msgstr "Klikněte na mapu pro přidání bodu trasy"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:413
msgid "Click to start drawing track"
msgstr "Kliknutím začnete kreslit stopu"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:414
msgid "Click to continue drawing track"
msgstr "Kliknutím pokračujte v kreslení trasy"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:415
msgid "Click last point to finish track"
msgstr "Klikněte na poslední bod pro dokončení trasy"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:417
msgid "Cancel drawing"
msgstr "Zrušit kreslení"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:418
msgid "Finish"
msgstr "Dokončit"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:419
msgid "Finish drawing"
msgstr "Dokončit kreslení"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:420
msgid "Delete last point"
msgstr "Odstranit poslední bod"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:421
msgid "Delete last drawn point"
msgstr "Odstranit poslední nakreslený bod"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:488
msgid "Track"
msgstr "Trasa"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:493
msgid "Route"
msgstr "Cesta"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:498
msgid "Waypoint"
msgstr "Bod trasy"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:503
msgid "Name"
msgstr "Název"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:504
msgid "Description"
msgstr "Popis"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:505
msgid "Comment"
msgstr "Komentář"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:506
msgid "Link text"
msgstr "Text odkazu"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:508
msgid "Link URL"
msgstr "URL odkazu"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:511
msgid "Lat"
msgstr "Zem. šířka"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:513
msgid "Lon"
msgstr "Zem. délka"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:515
msgid "Symbol"
msgstr "Symbol"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:518
msgid "No symbol"
msgstr "Bez symbolu"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:520
msgid "Unknown symbol"
msgstr "Neznámý symbol"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1007
msgid "Failed to save file"
msgstr "Nepodařilo se uložit soubor"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1025
msgid "File successfully saved as"
msgstr "Soubor úspěšně uložen jako"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1136
msgid "Impossible to load this file. "
msgstr "Tento soubor nelze načíst. "

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1137
msgid "Supported formats are gpx, kml, csv (unicsv) and jpg."
msgstr "Podporované formáty jsou gpx, kml, csv (unicsv) a jpg."

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1138
msgid "Load error"
msgstr "Chyba načítání"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1241
msgid "Tile server \"{ts}\" has been deleted"
msgstr "Byl odebrán mapový server \"{ts}\""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1244
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1248
msgid "Failed to delete tile server \"{ts}\""
msgstr "Nepodařilo se odebrat mapový server \"{ts}\""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1263
msgid "Server name or server url should not be empty"
msgstr "Název serveru nebo adresa serveru by neměly být prázdné"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1264
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1269
msgid "Impossible to add tile server"
msgstr "Nelze přidat mapový server"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1268
msgid "A server with this name already exists"
msgstr "Server s tímto názvem již existuje"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1334
msgid "Tile server \"{ts}\" has been added"
msgstr "Byl přidán mapový server \"{ts}\""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1337
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1341
msgid "Failed to add tile server \"{ts}\""
msgstr "Nepodařilo se přidat mapový server \"{ts}\""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1413
msgid "Failed to restore options values"
msgstr ""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1414
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1469
msgid "Error"
msgstr "Chyba"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1468
msgid "Failed to save options values"
msgstr ""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1535
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1542
#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1549
msgid "Impossible to write file"
msgstr "Není možné zapisovat soubor"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1536
msgid "write access denied"
msgstr "přístup k zápisu odepřen"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1543
msgid "folder does not exist"
msgstr "složka neexistuje"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1550
msgid "folder write access denied"
msgstr "přístup k zápisu do složky odepřen"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1556
msgid "Bad file name, must end with \".gpx\""
msgstr "Špatný název souboru, musí mít koncovku \".gpx\""

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1672
msgid "Load file (gpx, kml, csv, png)"
msgstr "Načíst soubor (gpx, kml, csv, png)"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1689
msgid "Load folder"
msgstr "Načíst složku"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1701
msgid "There is nothing to save"
msgstr "Není nic k uložení"

#: /var/www/html/n18/apps/gpxedit/js/gpxedit.js:1706
msgid "Where to save"
msgstr "Kam uložit"

#: /var/www/html/n18/apps/gpxedit/js/leaflet.js:5
msgid "left"
msgstr "vlevo"

#: /var/www/html/n18/apps/gpxedit/js/leaflet.js:5
msgid "right"
msgstr "vpravo"

#: /var/www/html/n18/apps/gpxedit/specialAppInfoFakeDummyForL10nScript.php:3
msgid " "
msgstr ""

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:8
msgid "Extra symbols"
msgstr "Další symboly"

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:9
msgid "Those symbols will be available in GpxEdit."
msgstr "Tyto symboly budou k dispozici v GpxEdit."

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:10
msgid "Keep in mind that only symbol names are saved in gpx files. Other programs will display default symbol if they do not know a symbol name."
msgstr "Mějte na paměti, že v gpx souborech jsou uloženy pouze názvy symbolů. Jiné programy budou zobrazovat výchozí symbol, pokud neznají název symbolu."

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:34
msgid "Recommended image ratio : 1:1"
msgstr "Doporučený poměr obrázku: 1:1"

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:35
msgid "Recommended image resolution : between 24x24 and 50x50"
msgstr "Doporučené rozlišení obrázku: mezi 24x24 a 50x50"

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:36
msgid "Accepted image format : png"
msgstr "Přijatelný formát obrázku: png"

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:38
msgid "New symbol name"
msgstr "Nový název symbolu"

#: /var/www/html/n18/apps/gpxedit/templates/admin.php:42
msgid "Upload new symbol image"
msgstr "Nahrát nový symbol"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:4
msgid "Load and save files"
msgstr "Načíst a uložit soubory"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:5
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:106
msgid "Options"
msgstr "Nastavení"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:6
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:316
msgid "About GpxEdit"
msgstr "O GpxEdit"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:24
msgid "Load file"
msgstr "Načíst soubor"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:25
msgid "Load directory"
msgstr "Načíst adresář"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:27
msgid "all files"
msgstr "všechny soubory"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:35
msgid "Save"
msgstr "Uložit"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:37
msgid "File name"
msgstr "Název souboru"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:40
msgid "Metadata"
msgstr "Metadata"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:42
msgid "Track name (optional)"
msgstr "Název stopy (volitelné)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:44
msgid "Description (optional)"
msgstr "Popis (nepovinný)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:46
msgid "Link text (optional)"
msgstr "Text odkazu (volitelné)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:48
msgid "Link URL (optional)"
msgstr "Odkaz (volitelné)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:51
msgid "Choose directory and save"
msgstr "Zvolte adresář a uložte"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:54
msgid "Clear map"
msgstr "Vymazat mapu"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:65
msgid "loading file"
msgstr "načítání souboru"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:69
msgid "exporting file to gpx"
msgstr "exportování souboru do gpx"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:73
msgid "saving file"
msgstr "ukládání souboru"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:109
msgid "Default symbol for waypoints when value is not set"
msgstr "Výchozí symbol pro body trasy, když není nastavena hodnota"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:110
msgid "Waypoint style"
msgstr "Styl trasového bodu"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:114
msgid "Tooltip"
msgstr "Popisek"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:116
msgid "on hover"
msgstr "při najetí kurzoru"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:117
msgid "permanent"
msgstr "trvale"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:119
msgid "Units (needs page reload to take effect)"
msgstr "Jednotky (vyžaduje obnovení stránky aby se projevilo)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:121
msgid "Metric"
msgstr "Metrický"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:122
msgid "English"
msgstr "Angličtina"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:123
msgid "Nautical"
msgstr "Námořní"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:125
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:127
msgid "Use defined symbols instead of default symbol"
msgstr "Použít definované symboly namísto výchozího symbolu"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:126
msgid "Use defined symbols"
msgstr "Použít definované symboly"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:129
msgid "Clear map before loading"
msgstr "Vymazat mapu před načtením"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:131
msgid "Approximate new points elevations"
msgstr ""

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:135
msgid "Custom tile servers"
msgstr ""

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:138
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:178
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:222
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:268
msgid "Server name"
msgstr "Název serveru"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:139
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:179
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:223
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:269
msgid "For example : my custom server"
msgstr "Například: můj vlastní server"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:140
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:180
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:224
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:270
msgid "Server url"
msgstr ""

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:141
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:225
msgid "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png"
msgstr ""

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:142
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:182
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:226
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:272
msgid "Min zoom (1-20)"
msgstr "Min. přiblížení (1 - 20)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:144
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:184
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:228
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:274
msgid "Max zoom (1-20)"
msgstr "Max přiblížení (1-20)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:146
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:190
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:236
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:286
msgid "Add"
msgstr "Přidat"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:149
msgid "Your tile servers"
msgstr "Vaše dlaždicové servery"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:175
msgid "Custom overlay tile servers"
msgstr "Vlastní servery pro překryvné vrstvy"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:181
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:271
msgid "For example : http://overlay.server.org/cycle/{z}/{x}/{y}.png"
msgstr "Například: http://overlay.server.org/cycle/{z}/{x}/{y}.png"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:186
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:276
msgid "Transparent"
msgstr "Průhledný"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:188
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:278
msgid "Opacity (0.0-1.0)"
msgstr "Průhlednost (0.0-1.0)"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:193
msgid "Your overlay tile servers"
msgstr "Vaše servery pro překryvné vrstvy"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:219
msgid "Custom WMS tile servers"
msgstr "Vlastní WMS dlaždicové servery"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:230
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:280
msgid "Format"
msgstr "Formát"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:232
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:282
msgid "WMS version"
msgstr "Verze WMS"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:234
#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:284
msgid "Layers to display"
msgstr "Vrstvy k zobrazení"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:239
msgid "Your WMS tile servers"
msgstr "Vaše WMS dlaždicové servery"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:265
msgid "Custom WMS overlay servers"
msgstr ""

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:289
msgid "Your WMS overlay tile servers"
msgstr ""

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:318
msgid "Features overview"
msgstr "Přehled funkcí"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:320
msgid "Draw, edition and deletion buttons are in the map's bottom-left corner."
msgstr "Tlačítka pro kreslení, úpravy a odstranění jsou v levém dolním rohu mapy."

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:322
msgid "You can draw a line or add a marker."
msgstr "Můžete nakreslit čáru nebo přidat značku."

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:324
msgid "If you click on a line or a marker, a popup pops and let you set the object properties."
msgstr "Pokud klepnete na čáru nebo značku, ukáže se vyskakovací okno a můžete nastavit vlastnosti objektu."

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:329
msgid "After a click on \"edition\" button, in edition mode, you can"
msgstr "Po kliknutí na tlačítko \"editace\" můžete v editačním režimu"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:332
msgid "move markers"
msgstr "přesunout značky"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:333
msgid "move line points"
msgstr "přesunout čárové body"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:334
msgid "click on a line point to remove it"
msgstr "kliknout na bod linie pro odstranění"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:335
msgid "hover a \"middle marker\" (between two line points) and press \"Del\" to cut the line in two (this action cannot be canceled)"
msgstr ""

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:338
msgid "Shortcuts"
msgstr "Zástupci"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:340
msgid "toggle sidebar"
msgstr "přepnout postranní panel"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:341
msgid "toggle minimap"
msgstr "přepnout minimapu"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:344
msgid "Documentation"
msgstr "Dokumentace"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:352
msgid "Source management"
msgstr "Správa zdrojů"

#: /var/www/html/n18/apps/gpxedit/templates/gpxcontent.php:363
msgid "Authors"
msgstr "Autoři"

